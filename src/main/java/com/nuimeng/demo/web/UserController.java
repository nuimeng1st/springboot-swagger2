package com.nuimeng.demo.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nuimeng.demo.po.User;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("user")
public class UserController {
	static Map<Integer, User> users = new HashMap<>();
	
	
	/**
	 * 创建用户
	 * @param user
	 * @return
	 */
	@ApiOperation(value="创建用户",notes="根据User用户创建用户")
	@ApiImplicitParam(name="user",value="用户详细实体user",required=true,dataType="User")
	@PostMapping("add")
	public String add(@RequestBody User user) {
		users.put(user.getId(), user);
		return "user add: "+user.getName()+" success!";
	}
	/**
	 * 删除用户
	 * @param id
	 * @return
	 */
	@ApiOperation(value="删除用户",notes="根据用户id删除用户")
	@ApiImplicitParam(name="id",value="用户id",required=true,dataType="Integer")
	@DeleteMapping("del")
	public String delete(Integer id) {
		users.remove(id);
		return "user del: "+users.get(id).getName()+" success!";
	}
	/**
	 * 修改用户
	 * @param user
	 * @return
	 */
	@ApiOperation(value="更新用户",notes="根据新的用户信息更新用户")
	@ApiImplicitParam(name="user",value="新用户实体信息",required=true,dataType="User")
	@PutMapping("put")
	public String put(@RequestBody User user) {
		users.put(user.getId(), user);
		return "user put: "+user.getName()+" success!";
	}
	/**
	 * 查询用户
	 * @return
	 */
	@ApiOperation(value="获取用户列表")
	@GetMapping("list")
	public List<User> list() {
		return new ArrayList<>(users.values());
	}
	
}
